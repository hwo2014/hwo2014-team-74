package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import Protocol._

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))

  send(MsgWrapper("join", Join(botName, botKey)))
  play()

  @tailrec private def play() {
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("carPositions", environment) =>
          val env = Extraction.extract[List[CarPosition]](environment)
          send(MsgWrapper("throttle", 0.5))
          println(env)

        case MsgWrapper("gameInit", gameInit) =>
          val race = Extraction.extract[GameInit](gameInit)
          println(race.race.track)

        case MsgWrapper("yourCar", yourCar) =>
          val myCar = Extraction.extract[CarId](yourCar)
          println(myCar)

        case MsgWrapper("turboAvailable", _) =>
          // send(MsgWrapper("turbo", "Fuck off you!! We get turbo!!"))

        case MsgWrapper("gameEnd", result) =>
          val myResult = Extraction.extract[GameEnd](result)
          println(myResult)

        case MsgWrapper(msgType, _) =>
          send(MsgWrapper("throttle", 0.5))
          println("Received: " + msgType)
      }
      play()
    }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush()
  }

}


