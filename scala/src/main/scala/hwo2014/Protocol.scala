package hwo2014

import org.json4s._

object Protocol {

  case class Join(name: String, key: String)

  case class MsgWrapper(msgType: String, data: JValue) {
  }

  object MsgWrapper {
    implicit val formats = new DefaultFormats{}

    def apply(msgType: String, data: Any): MsgWrapper = {
      MsgWrapper(msgType, Extraction.decompose(data))
    }
  }

  case class CarId(name: String, color: String)

  case class Piece(length: Double, switch: Boolean, radius: Double, angle: Double) {
    def this(length: Double) = this(length, false, 0.0, 0.0)
    def this(length: Double, switch: Boolean) = this(length, switch, 0.0, 0.0)
    def this(radius: Double, angle: Double) = this(radius * angle, false, radius, angle)
  }

  case class Lane(distanceFromCenter: Int, index: Int)

  case class Position(x: Double, y: Double)

  case class StartingPoint(position: Position, angle: Double)

  case class Track(
      id: String,
      name: String,
      pieces: List[Piece],
      lanes: List[Lane],
      startingPoint: StartingPoint)

  case class Dimensions(length: Double, width: Double, guideFlagPosition: Double)

  case class Car(id: CarId, dimensions: Dimensions)

  case class RaceSession(laps: Int, maxLapTimeMs: Int, quickRace: Boolean)

  case class Race(track: Track, cars: List[Car], raceSession: RaceSession)

  case class GameInit(race: Race)

  case class SwitchLanes(startLaneIndex: Int, endLaneIndex: Int)

  case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: SwitchLanes, lap: Int)

  case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition)

  case class Result(laps: Int, ticks: Int, millis: Int)

  case class BestLap(lap: Int, ticks: Int, millis: Int)

  case class ResultPerCar(car: CarId, result: Result)

  case class BestLapPerCar(car: CarId, result: BestLap)

  case class GameEnd(results: List[ResultPerCar], bestLaps: List[BestLapPerCar])
}
